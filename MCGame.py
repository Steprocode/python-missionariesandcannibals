"""
Logic game - Missionaries and Cannibals.
You have to move all of the characters
to the left side using raft. Note, that you
cannot leave missionaries on any coast,
when they are minority among cannibals. Move
characters and the raft by clicking on them.

Created by Maciej Stępień
"""

import pygame
import sys
import random
import math


def handleInput():
    """
    Adds detected events to the global
    list of events. Method should be called
    in every tick.
    """

    global events
    events = []
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            events.append(pygame.MOUSEBUTTONUP)
        if event.type == pygame.QUIT:
            sys.exit()
    keys = pygame.key.get_pressed()

    if keys[pygame.K_s]:
        events.append(pygame.K_s)
    if keys[pygame.K_w]:
        events.append(pygame.K_w)
    if keys[pygame.K_UP]:
        events.append(pygame.K_UP)
    if keys[pygame.K_DOWN]:
        events.append(pygame.K_DOWN)
    if keys[pygame.K_SPACE]:
        events.append(pygame.K_SPACE)
    if keys[pygame.K_ESCAPE]:
        sys.exit()


def drawText(text, pos, maxWidth, maxHeight):
    """
    Draws text on the screen automatically divided
    by lines in the given position.
    """

    global scr, font, white, events
    words = [word.split(' ') for word in text.splitlines()]
    space = font.size(' ')[0]
    x, y = pos
    for line in words:
        for word in line:
            wordSurf = font.render(word, 0, white)
            wordWidth, wordHeight = wordSurf.get_size()
            if x + wordWidth >= maxWidth:
                x = pos[0]
                y += wordHeight
            scr.blit(wordSurf, (x, y))
            x += wordWidth + space
        x = pos[0]
        y += wordHeight


def showIntroScreen():
    """
    Displays introduction screen with game instruction.
    """

    global scr, font
    text = "You have to move all of the characters \
    to the left side using raft. Note, that you \
    cannot leave missionaries on any coast, \
    when they are minority among cannibals. Move \
    characters and the raft by clicking on them."

    background = pygame.image.load("background.png")
    backgroundBox = background.get_rect()
    backgroundBox.center = scr.get_rect().center
    bounds = scr.get_rect()
    bounds.width = int(0.9 * bounds.width)
    bounds.midtop = scr.get_rect().midtop
    while True:
        handleInput()
        if pygame.K_SPACE in events:
            showPlayscreen()

        scr.blit(background, backgroundBox)
        drawText(text,
                 bounds.topleft,
                 bounds.width,
                 bounds.height)
        drawText("Press space to start game",
                 (bounds.left, 440),
                 bounds.width,
                 bounds.height)
        pygame.display.flip()
        clock.tick(60)


def showPlayscreen():
    """Displays and starts game"""

    def checkStatus():
        """
        Returns current status of the game,
        which can be running, failure and
        success.
        """

        nonlocal state, raft, gamegraph

        if gamegraph.get(gamestate) == "failure":
            return "failure"
        elif gamegraph.get(gamestate) == "success":
            return "success"
        else:
            return "running"

    def checkClickedObject():
        """
        Returns selected object, which can be
        missionary, cannibal and raft. If none of
        them is selected, then returns None.
        """

        global events
        nonlocal state, raft
        click = pygame.mouse.get_pressed()
        mousePos = pygame.mouse.get_pos()
        selected = None

        if pygame.MOUSEBUTTONUP in events:
            if raft.get("box").collidepoint(mousePos):
                selected = raft

            for entity in state.get("raft"):
                box = entity.get("box")
                if box.collidepoint(mousePos):
                    selected = entity
            for entity in state.get("right_side"):
                box = entity.get("box")
                if box.collidepoint(mousePos):
                    selected = entity
            for entity in state.get("left_side"):
                box = entity.get("box")
                if box.collidepoint(mousePos):
                    selected = entity

        return selected

    def updateStatus(dt, state):
        """
        Checks position of a raft and updates it.
        If raft arrived to the coast, then gamestate
        is updated.
        """

        nonlocal spawnPoints, raft, river, gamestate
        canibals = 0
        missionaries = 0

        if raft.get("position") == "transient_left":
            raft["box"] = raft.get("box").move((int)(-400 * dt), 0)
            if raft.get("box").left <= river.left:
                raft.get("box").left = river.left
                raft["position"] = "left_side"

                for entity in state.get("raft"):
                    if entity.get("type") == "cannibal":
                        canibals += 1
                    elif entity.get("type") == "missionary":
                        missionaries += 1
        elif raft.get("position") == "transient_right":
            raft["box"] = raft.get("box").move((int)(400 * dt), 0)
            if raft.get("box").right >= river.right:
                raft.get("box").right = river.right
                raft["position"] = "right_side"

                for entity in state.get("raft"):
                    if entity.get("type") == "cannibal":
                        canibals += 1
                    elif entity.get("type") == "missionary":
                        missionaries += 1
        key = ""
        for n in range(0, canibals):
            key += "c"
        for n in range(0, missionaries):
            key += "m"

        if key != "":
            gamestate = gamegraph.get(gamestate).get(key)

    def drawScene(dt, state):
        """Draws all characters and the raft."""

        nonlocal spawnPoints, raft, river, gamestate

        scr.blit(raft.get("image"), raft.get("box"))
        iterator = 0
        for entity in state.get("right_side"):
            nonEmptyPositionIndex = iterator + 6 \
                                    - len(state.get("right_side"))
            box = entity.get("box")
            box.midbottom = \
                spawnPoints.get("right_side")[nonEmptyPositionIndex]
            scr.blit(entity.get("image"), box)
            iterator += 1

        iterator = 0
        for entity in state.get("left_side"):
            nonEmptyPositionIndex = iterator + 6 \
                                    - len(state.get("left_side"))
            box = entity.get("box")
            box.midbottom = \
                spawnPoints.get("left_side")[nonEmptyPositionIndex]
            scr.blit(entity.get("image"), box)
            iterator += 1

        iterator = 0
        for entity in state.get("raft"):
            box = entity.get("box")
            box.midbottom = (
                spawnPoints.get("raft")[iterator][0] + raft.get("box").left,
                spawnPoints.get("raft")[iterator][1] + raft.get("box").top
                )
            scr.blit(entity.get("image"), box)
            iterator += 1

    def loadEntity(name, image):
        """
        Returns dictionary with properties of
        entity.
        """

        entity = {
            "type": name,
            "image": image,
            "box": image.get_rect()
        }
        return entity

    def positionRaft(raft, river):
        """Sets position of the raft."""

        raftBox = raft.get("box")
        raftBox.center = river.center

        if raft.get("position") == "right_side":
            raftBox.right = river.right
        elif raft.get("position") == "left_side":
            raftBox.left = river.left

    def loadRaft():
        """Returns dictionary with properties of
        the raft."""

        nonlocal river
        image = pygame.image.load("raft.png")
        raft = {
            "type": "raft",
            "image": image,
            "box": image.get_rect(),
            "position": "right_side"
        }
        positionRaft(raft, river)
        return raft

    def drawTime(time):
        global scr, white, font
        minutes = int(time / 60)
        seconds = int(time - minutes * 60)

        text = str(minutes).zfill(2)
        text += ":"
        text += str(seconds).zfill(2)

        timeText = font.render(text, True, white)
        timeBox = successText.get_rect()

        timeBox.center = scr.get_rect().center
        timeBox.top = scr.get_rect().top

        scr.blit(timeText, timeBox)

    def moveEntity(entity):
        """Moves entity between coast and the raft."""
        nonlocal state
        nonlocal raft

        if entity in state.get("raft"):
            if raft.get("position") == "right_side":
                state.get("raft").remove(entity)
                state.get("right_side").append(entity)
            elif raft.get("position") == "left_side":
                state.get("raft").remove(entity)
                state.get("left_side").append(entity)
        elif entity in state.get("right_side"):
            if raft.get("position") == "right_side" \
                    and len(state.get("raft")) < 2:
                state.get("right_side").remove(entity)
                state.get("raft").append(entity)
        elif entity in state.get("left_side"):
            if raft.get("position") == "left_side" \
                    and len(state.get("raft")) < 2:
                state.get("left_side").remove(entity)
                state.get("raft").append(entity)

    # positions, where characters can be
    spawnPoints = {
        "left_side": ((70, 500 - 300),
                      (190, 500 - 280),
                      (80, 500 - 200),
                      (170, 500 - 170),
                      (70, 500 - 60),
                      (170, 500 - 30)),
        "right_side": ((770, 500 - 300),
                       (830, 500 - 270),
                       (690, 500 - 190),
                       (820, 500 - 170),
                       (710, 500 - 80),
                       (810, 500 - 30)),
        "raft": ((60, 60),
                 (90, 140))
    }

    river = pygame.Rect(260, 500 - 220, 340, 220)

    # init font, success and failure texts
    successText = font.render("SUCCESS", True, white)
    successBox = successText.get_rect()
    successBox.center = scr.get_rect().center
    failureText = font.render("FAILURE", True, white)
    failureBox = failureText.get_rect()
    failureBox.center = scr.get_rect().center

    background = pygame.image.load("background.png")
    backgroundBox = background.get_rect()

    missionaryImage = pygame.image.load("missionary.png")
    cannibalImage = pygame.image.load("cannibal.png")

    raft = loadRaft()

    # state is current set of positions of real character objects
    state = {
        "left_side": [],
        "right_side": [loadEntity("cannibal", cannibalImage),
                       loadEntity("cannibal", cannibalImage),
                       loadEntity("cannibal", cannibalImage),
                       loadEntity("missionary", missionaryImage),
                       loadEntity("missionary", missionaryImage),
                       loadEntity("missionary", missionaryImage)],
        "raft": []
    }

    # allowed positions of characters
    gamegraph = {
        "-cccmmmr": {"c": "cr-ccmmm",
                     "m": "mr-cccmm",
                     "cm": "cmr-ccmm",
                     "mm": "mmr-cccm",
                     "cc": "ccr-cmmm"},

        "cr-ccmmm": {"c": "-cccmmmr"},
        "c-ccmmmr": {"c": "ccr-cmmm",
                     "m": "cmr-ccmm",
                     "cm": "ccmr-cmm",
                     "mm": "cmmr-ccm",
                     "cc": "cccr-mmm"},

        "mr-cccmm": "failure",
        "m-cccmmr": "failure",

        "cmr-ccmm": {"c": "m-cccmmr",
                     "m": "c-ccmmmr",
                     "cm": "-cccmmmr"},
        "cm-ccmmr": {"c": "ccmr-cmm",
                     "m": "cmmr-ccm",
                     "cm": "ccmmr-cm",
                     "cc": "cccmr-mm",
                     "mm": "cmmmr-cc"},

        "mmr-cccm": "failure",
        "mm-cccmr": "failure",

        "ccr-cmmm": {"c": "c-ccmmmr",
                     "cc": "-cccmmmr"},
        "cc-cmmmr": {"c": "cccr-mmm",
                     "m": "ccmr-cmm",
                     "cm": "cccmr-mm",
                     "mm": "ccmmr-cm"},

        "cccr-mmm": {"c": "cc-cmmmr",
                     "cc": "c-ccmmmr"},
        "ccc-mmmr": {"m": "cccmr-mm",
                     "mm": "cccmmr-m"},

        "ccmr-cmm": "failure",
        "ccm-cmmr": "failure",

        "cmmr-ccm": "failure",
        "cmm-ccmr": "failure",

        "mmmr-ccc": {"m": "mm-cccmr",
                     "mm": "m-cccmmr"},
        "mmm-cccr": {"c": "cmmmr-cc",
                     "cc": "ccmmmr-c"},

        "cccmr-mm": "failure",
        "cccm-mmr": "failure",

        "ccmmr-cm": {"c": "cmm-ccmr",
                     "m": "ccm-cmmr",
                     "cm": "cm-ccmmr",
                     "cc": "mm-cccmr",
                     "mm": "cc-cmmmr"},
        "ccmm-cmr": {"c": "cccmmr-m",
                     "m": "ccmmmr-c",
                     "cm": "cccmmmr-"},

        "cmmmr-cc": {"c": "mmm-cccr",
                     "m": "cmm-ccmr",
                     "cm": "mm-cccmr",
                     "mm": "cm-ccmmr"},
        "cmmm-ccr": {"c": "ccmmmr-c",
                     "cc": "cccmmmr-"},

        "cccmmr-m": "failure",
        "cccmm-mr": "failure",

        "ccmmmr-c": {"c": "cmmm-ccr",
                     "m": "ccmm-cmr",
                     "cm": "cmm-ccmr",
                     "cc": "mmm-cccr",
                     "mm": "ccm-cmmr"},
        "ccmmm-cr": {"c": "cccmmmr-"},

        "cccmmmr-": "success"
    }

    # current positions of characters
    gamestate = "-cccmmmr"

    dt = 0
    time = 0
    while True:
        handleInput()
        status = checkStatus()

        if status == "running":
            time += dt
            # handle player input
            selected = checkClickedObject()
            if selected is not None:
                if selected.get("type") == "raft" \
                        and len(state.get("raft")) > 0:
                    if "left_side" in raft.get("position"):
                        raft["position"] = "transient_right"
                    elif "right_side" in raft.get("position"):
                        raft["position"] = "transient_left"
                else:
                    if "transient" not in raft.get("position"):
                        moveEntity(selected)
        else:
            if pygame.K_SPACE in events:
                showPlayscreen()

        updateStatus(dt, state)

        # draw background, elements and gui
        scr.fill(black)
        scr.blit(background, backgroundBox)
        drawScene(dt, state)
        drawTime(time)
        if status == "failure":
            scr.blit(failureText, failureBox)
        elif status == "success":
            scr.blit(successText, successBox)

        pygame.display.flip()
        dt = clock.tick(60)
        dt /= 1000
    return


if __name__ == "__main__":
    global black, white, scr, clock, modeType, events, font
    events = []
    pygame.init()
    black = (0, 0, 0)
    white = (255, 255, 255)
    scr = pygame.display.set_mode((900, 500))
    font = pygame.font.Font('freesansbold.ttf', 48)
    pygame.key.set_repeat(1, 1)
    clock = pygame.time.Clock()
    showIntroScreen()
