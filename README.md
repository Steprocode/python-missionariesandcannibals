# Python-MissionariesAndCannibals
A simple Missionaries and Cannibals game written in Python3 using Pygame.

## Table of contents
* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
	
## Description
The aim of a player is to move both missionaries and cannibals through a river to the other coast using ferry. Number of cannibals on one coast cannot be greater than the number of missionaries.

## Dependencies
Program requires following libraries to be installed:

* pygame
	
## Setup
Download Pygame:
```
$ pip3 install pygame
```

Run the program:
```
$ python3 MCGame.py
```
